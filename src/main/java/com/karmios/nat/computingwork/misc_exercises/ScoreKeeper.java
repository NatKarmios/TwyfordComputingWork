package com.karmios.nat.computingwork.misc_exercises;

import static com.karmios.nat.computingwork.utils.Utils.inputBoolLoop;
import static com.karmios.nat.computingwork.utils.Utils.inputIntLoop;

public class ScoreKeeper {
    public static void main(String[] args) {
        int playerOneScore = 0;
        int playerTwoScore = 0;
        int noOfGamesInMatch = inputIntLoop("How many games?");
        for (int noOfGamesPlayed = 0; noOfGamesPlayed < noOfGamesInMatch; noOfGamesPlayed++) {
            if (inputBoolLoop("Did Player One win the game?", true)) playerOneScore ++;
            else playerTwoScore++;
        }
        System.out.println(playerOneScore);
        System.out.println(playerTwoScore);
    }
}
