package com.karmios.nat.computingwork.misc_exercises

import com.karmios.nat.computingwork.utils.Utils.inputBoolLoop
import com.karmios.nat.computingwork.utils.Utils.inputIntLoop

fun main(args: Array<String>) {
    var (playerOneScore, playerTwoScore) = Pair(0, 0)
    for (i in 0 until inputIntLoop("How many games?")) {
        if (inputBoolLoop("Did Player One win the game?", true)) playerOneScore++
        else playerTwoScore++
    }
    println(playerOneScore)
    println(playerTwoScore)
}
