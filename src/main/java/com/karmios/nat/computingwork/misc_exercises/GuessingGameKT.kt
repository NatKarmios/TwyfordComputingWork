package com.karmios.nat.computingwork.misc_exercises

import com.karmios.nat.computingwork.utils.Utils.inputIntLoop

const val MAX_GUESSES = 5

fun main(args: Array<String>) {
    val numberToGuess = inputIntLoop("Player One enter your chosen number: ",
                                     "Not a valid choice, please enter another number.")
                                    { it in 1..10 }
    for (i in 0 until MAX_GUESSES) {
        val guess = inputIntLoop("Player Two have a guess: ",
                                 "Not a valid choice, please enter another number.")
                                { it in 1..10 }
        if (guess == numberToGuess) {
            println("Player Two Wins")
            return
        }
    }
    println("Player One Wins")

}