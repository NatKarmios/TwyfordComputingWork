package com.karmios.nat.computingwork.misc_exercises;

import static com.karmios.nat.computingwork.utils.Utils.inBounds;
import static com.karmios.nat.computingwork.utils.Utils.inputIntLoop;

public class GuessingGame {
    public static void main(String[] args) {
        int numberToGuess = inputIntLoop("Player One enter your chosen number: ",
                                         "Not a valid choice, please enter another number",
                                         x -> x>0 && x<11);

        for (int numberOfGuesses = 0; numberOfGuesses < 5; numberOfGuesses++) {
            int guess = inputIntLoop("Player Two have a guess: ",
                                             "Not a valid choice, please enter another number",
                                             inBounds(1, 11));
            if (guess == numberToGuess) {
                System.out.println("Player Two Wins");
                return;
            }
        }
        System.out.println("Player One Wins");
    }
}
