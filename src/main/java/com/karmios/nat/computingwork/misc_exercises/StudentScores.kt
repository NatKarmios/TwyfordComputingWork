package com.karmios.nat.computingwork.misc_exercises

import com.karmios.nat.computingwork.utils.Utils.input
import com.karmios.nat.computingwork.utils.Utils.inputIntLoop

data class Student(val name: String, val score: Int)

fun main(args: Array<String>) {
    val students = generateSequence({ Student(
            name = input("Enter student name: "),
            score = inputIntLoop("Enter score: ", "Score must be between 0 and 100!") {it in 0..100}
    ) }).take(inputIntLoop("Enter how many students: ", "Not a valid number!") { it >= 0 }).toList()

    val topScorer = students.maxBy { it.score }
    if (topScorer!=null) println("The highest scoring student is ${topScorer.name} (${topScorer.score}).");
}
