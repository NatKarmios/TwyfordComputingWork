package com.karmios.nat.computingwork.misc_exercises

import com.karmios.nat.computingwork.utils.Utils.inputIntLoop

fun <T> List<T>.subList(endIndex: Int): List<T>  = this.subList(0, endIndex)

fun main(args: Array<String>) {
    val isbn = generateSequence {
        inputIntLoop("Please enter next digit of ISBN: ",
                     "Must be a single digit!") { it in 0..9 }
    }.take(13).toList()

    val calculatedDigit = (10 - (isbn.subList(12).mapIndexed {index, it -> when {
        index%2 == 1 -> it*3
        else -> it
    } }.sum() % 10)) % 10

    println("${if (calculatedDigit == isbn[12]) "V" else "Inv"}alid ISBN")
}
