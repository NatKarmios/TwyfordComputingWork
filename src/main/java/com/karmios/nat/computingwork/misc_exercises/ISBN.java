package com.karmios.nat.computingwork.misc_exercises;

import static com.karmios.nat.computingwork.utils.Utils.inBounds;
import static com.karmios.nat.computingwork.utils.Utils.inputIntLoop;
import static java.util.stream.IntStream.range;

public class ISBN {
    public static void main(String[] args) {
        int[] isbn = new int[13];
        range(0, 13).forEach(i -> isbn[i] = inputIntLoop("Please enter next digit of ISBN: ",
                                                         "Must be a single digit!",
                                                         inBounds(10)));

        int calculatedDigit = 0;
        int count = 0;
        while (count < 12) {
            calculatedDigit += isbn[count++];
            calculatedDigit += isbn[count++]*3;
        }
        calculatedDigit = calculatedDigit % 10;
        calculatedDigit = 10 - calculatedDigit;
        if (calculatedDigit == 10) calculatedDigit = 0;
        System.out.println((calculatedDigit == isbn[12] ? "Valid" : "Invalid") + " ISBN");
    }
}
