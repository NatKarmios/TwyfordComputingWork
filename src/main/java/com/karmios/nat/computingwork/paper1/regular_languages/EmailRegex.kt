package com.karmios.nat.computingwork.paper1.regular_languages

val pattern = Regex("^[a-zA-Z][a-zA-Z\\d]*@[a-zA-Z][a-zA-Z\\d]*\\.[a-zA-Z\\d]+$")

fun main(args: Array<String>) {
    print("Enter your email: ");
    println(if (pattern.matches(readLine() ?: "")) "Your email is valid." else "Your email is invalid!");
}
