package com.karmios.nat.computingwork.wordswithaqa;

import java.util.Random;

public class QueueOfTiles {
     protected char[] contents;
        protected int rear;
        protected int maxSize;
        
        QueueOfTiles(int maxSize)
        {
            contents = new char[maxSize];
            rear = -1;
            this.maxSize = maxSize;
            for (int count = 0; count < maxSize; count++)
            { 
                add();
            }
        }

        boolean isEmpty()
        {
            if (rear == -1) 
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        
        char remove()
        {
            if (isEmpty()) {
                return '\n';
            }
            else
            {
                char item = contents[0];
                for (int count = 1; count < rear + 1; count++) {
                    contents[count - 1] = contents[count];
                }
                contents[rear] = '\n';
                rear -= 1;
                return item;
            }
        }
        
        void add()
        {
            Random rnd = new Random();
            if (rear < maxSize - 1) 
            {
                int randNo = rnd.nextInt(25);
                rear += 1;
                contents[rear] = (char)(65 + randNo);
            }
        }
        
        void show()
        {
            if (rear != -1) 
            {
                Console.println();
                Console.print("The contents of the queue are: ");
                for(char item : contents)
                {
                    Console.print(item);
                }
                Console.println();
            }
        }
}
