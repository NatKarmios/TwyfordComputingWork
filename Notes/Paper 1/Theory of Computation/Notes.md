# Computational Theory


## Problem solving

**Proposition:** A statement that can resolve to a boolean,
e.g. *"Today is Friday"*.

**Compound Proposition:** Multiple propositions joined by an operator,
e.g. *"It snows only if it is cold."*

**Conditional Connective:** If P is only true when Q is true, then P => Q.

**Fundamental Principle of Logical Reasoning:** P => Q, Q => R ⊢ P => R

**Newton's Principle:** *"Whatever is true of everything before our eyes is
true of everything in the universe,"* e.g. *"If x is a footballer, then
x wears football boots when playing football."*


## Following and writing algorithms

**Algorithm:** A precise set of instructions used to solve a problem.

Algorithms can be difficult design because:
- They must be unambiguous
- They must achieve the same result
- They must work correctly for all possible inputs / problem instances 
- They must finish in a finite time
- There may be more problem instances than is possible to test for

*"The following are sufficient for constructing algorithms:"*
- sequence
- assignment
- selection
- iteration

Pseudo-code is often used to express a problem,
as it is unambiguous and does not rely on a 
specific language's syntax.

**Correctness:** Whether a program will give 
the correct output for any possible input.


## Classification of Algorithms

**Tractable problem:** a problem that can be solved in polynomial time
(`O(n^k)`) or better

**Intractable problem:** a problem which is (maybe) only solvable with
an algorithm that is worse than `O(n^k)`.
 
**Heuristics:** A faster method that achieves an approximate solution.

Some heuristics relax the problem's constraints to get a solution;
the solution may not be optimal or complete, but it will be achieved
much faster than an exhaustive solution.

| Tractable                                       | Intractable       |
| ----------------------------------------------- | ------------------|
| `O(1)`, `O(logn)`, `O(n)`, `O(n^k)`, `O(nlogn)` | `O(k^n)`, `O(n!)` |


## A Model of Computation (Turing Machine)

A turing machine is a simple machine with a control unit, a read-write
head, and an infinitely-long piece of tape.

**Transition Function:**
`(current state, input symbol) = (next state, output symbol, movement)`

 - *If output state is omitted, then no output is produced*
 - *If movement is omitted, then the program stops* 

