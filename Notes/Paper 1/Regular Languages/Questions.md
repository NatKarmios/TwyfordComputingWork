# Regular Languages


## Maths for regular expressions

#### Page 408

1a\) `{2, 4, 6, 9}`
<br>
1b\) `{1, 2, 6, 7, 8}`

2\) `{x | x∈ℤ ^ x<=10}`

3\) 4, 9, 16, 25, 36


#### Page 410

4\) A set with a finite number of elements

5\) A set with an infinite number of elements, that can be placed in
    1:1 correspondence with the natural numbers.

6\) The set of all square numbers, the set of all powers of 2


#### Page 411

7a\) 4
<br>
7b\) 3
<br>
7c\) 0


#### Page 413

8\) The set of all possible combinations of lockers and students

9\) `{(a, 1), (a, 2), (a, 3), (b, 1), (b, 2), (b, 3)}`

10\) The set of ll the possible flights, with any airline from any
     airport to another airport

11\) The set of the coordinates of every pixel on the display


#### Page 414

12 \(a\) T \(b\) T \(c\) T \(d\) F \(e\) T

13 \(a\) `{1, 2, 3, 4}` \(b\) `{1, 2, 3, 4}` \(c\) `{2}`
   \(d\) `{}`  \(e\) `{4, 8}` \(f\) `{}`


## Regular Expressions

#### Page 417

1\) `000`, `001`, `010`, `011`, `100`, `101`, `110,`, `111`

2\) `000`, `010`, `101`, `111`


#### Page 418

3 \(a\) T \(b\) T \(c\) F \(d\) F


#### Page 419

4 \(a\) T \(b\) T \(c\) F \(d\) F \(e\) F

5\) `(A|C|G|T)+`

6\) `(A|C|G|T|a|c|g|t)+`


#### Page 420

7 <br>
a\) Any letter followed by 0 or more letters, digits and
underscores.
<br>
b\) \(i\) T \(ii\) T \(iii\) F \(iv\) T

8 <br>
a\) \(i\) T \(ii\) T \(iii\) F \(iv\) F
<br>
b\) 31/02/2001

9 <br>
a\) The backslash escapes the `+` character,
    making it a literal `+` in the expression.
<br>
b\) The fist backslash escapes the second backslash, resulting
    in a *single*, literal backslash in the expression.
<br>
c\) `[a-d]` represents any letter from `a` to `d`, whereas
     `[a\-d]` represents any character in the set `{'a', '-', 'd'}`


## Context-Free Grammar

#### Page 434

7 <br>
a\) Syntax diagram
<br>
b\)

| Construct            | Example                             | Valid (Yes/No) |
| -------------------- | ----------------------------------- | -------------- |
| identifier           | MyCount3                            | No             |
| variable declaration | x : integer;                        | No             |
| variable declaration | var x, y : integer; flag : boolean; | Yes            |

c \)<br>
`<variable declaration> ::= var <variable declaration body>`
<br>
`<variable declaration body> ::= <identifier list> : <type> ; 
                               | <identifier list> : <type> ; <variable declaration body>` 
<br>
`<identifier list> ::= <identifier> | <identifier> , <identifier list>`
<br>
`<identifier> ::= <letter> | <letter> <identifier>`
<br>
`<type> ::= integer | float | boolean | string`